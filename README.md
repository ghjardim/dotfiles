# ./dotfiles

![Desktop screenshot](pics/scrshot1.png)

### What are dotfiles?
Dotfiles are configuration files that are used to customize your Linux installation. They're called this way because there is a period at the beginning of the file name (i.e. it is a hidden file or directory), which looks like a dot.
This repo contains some of my config files. They are stored here for convenience, so I can quickly access them on new machines or new installations. In addition, others may find some of my settings useful when customizing their own dotfiles.

### Current setup
Here is a list of packages I use. Not all of them have configuration files in this repo:

- **Debian Trixie** (testing) distro
- **bash** shell
- **bpswm** window manager
- **rofi** menu / app launcher
- **xfce4-terminal** terminal emulator
- **lf, spacefm** file managers
- **vim** text editor
- **qutebrowser** web browser
- **newsboat** rss feed reader
- **mpv** video player
- **feh, sxiv** image viewer
- **polybar** bar
- **picom** compositor

### Aesthetics information
- **Colorscheme**: oliver-dark (based in base64-dark)
- **GTK Theme**: pure-dark
- **Icon theme**: Gruvbox-Material-Dark
- **Qt5 Scheme**: darker (set with qt5ct)
- **Cursor theme**: Breeze

### Main features
- Some rofi menu scripts [here](.config/rofi) (for editing my configs, taking screenshots and watching Twitch livestreams)
- Minimalist [zsh config](.config/zsh/.zshrc), created without any framework. ZSH loads in 3 seconds.
- XDG-compliant setup using my [environment configuration](.config/env) (more info at the [Arch Wiki](https://wiki.archlinux.org/title/XDG_Base_Directory)).
- [keynavrc](.config/keynav/keynavrc), for when you don't want to use the mouse
