# ~/.bashrc
#
# shellcheck source=/dev/null

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Options
HISTSIZE=
HISTFILESIZE=
shopt -s cmdhist # save multi-line commands in history as single line
shopt -s histappend # do not overwrite history
shopt -s expand_aliases # expand aliases
PROMPT_COMMAND="history -a; history -n; $PROMPT_COMMAND" # immeditely write on history

# PS1
PS1=
PS1+='\[\033[0;34m\]$\[\033[0m\]'
PS1+=' [\[\033[0;34m\]\W\[\033[0m\]]'
PS1+=' $(git branch &>/dev/null \
    && git branch 2>/dev/null \
    | sed -n "s/* \(.*\)/(\[\[\033[0;34m\]\1\[\033[0m\]\) /p")'
PS1+=': '

# Completion
source ~/.config/shell/completions/doas
source ~/coding/codeberg/notpublished/kanban.bash/kanban.completion
complete -cf command

# Colors for TTY
[ "$TERM" = "linux" ] && {
    echo -en "\e]P0"'000000' #black
    echo -en "\e]P8"'585858' #darkgrey
    echo -en "\e]P1"'ab4642' #darkred
    echo -en "\e]P9"'ab4642' #red
    echo -en "\e]P2"'a1b56c' #darkgreen
    echo -en "\e]PA"'a1b56c' #green
    echo -en "\e]P3"'f7ca88' #brown
    echo -en "\e]PB"'f7ca88' #yellow
    echo -en "\e]P4"'7cafc2' #darkblue
    echo -en "\e]PC"'7cafc2' #blue
    echo -en "\e]P5"'ba8baf' #darkmagenta
    echo -en "\e]PD"'ba8baf' #magenta
    echo -en "\e]P6"'86c1b9' #darkcyan
    echo -en "\e]PE"'86c1b9' #cyan
    echo -en "\e]P7"'d8d8d8' #lightgrey
    echo -en "\e]PF"'f8f8f8' #white
    clear #for background artifacting
}

# Sourcing
source ~/.config/env
source ~/.config/shell/aliasrc
source ~/.config/shell/functions
[ -a ~/.config/shell/private ] && source ~/.config/shell/private

# Header
source ~/.config/shell/greeting

# Bash-specific functions
mp4split(){
    filename=$1

    idioma1=$(ffprobe -v error -select_streams a:0 -show_entries stream_tags=language -of default=noprint_wrappers=1:nokey=1 $1)
    ffmpeg -i $filename -map 0:v -map 0:a:0 -c copy -shortest "${filename%mp4}$idioma1.mp4"

    idioma2=$(ffprobe -v error -select_streams a:1 -show_entries stream_tags=language -of default=noprint_wrappers=1:nokey=1 $1)
    ffmpeg -i $filename -map 0:v -map 0:a:1 -c copy -shortest "${filename%mp4}$idioma2.mp4"
}


# Cemetery of dead PS1s
#PS1="${BLUE}\u ${WHITE}at ${BLUE}\W ${WHITE}> ${RESET}"
#PS1='\[[94m\]\u \[[97m\]at \[[94m\]\W\[[97m\] $(git branch &>/dev/null && git branch 2>/dev/null | sed -n "s/^/on \[[94m\]/;s/* \(.*\)/\1 /p" | sed "s/$/branch /")\[[97m\]> \[(B[m\]' # LG
