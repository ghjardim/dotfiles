#!/bin/bash


rofi_path=rofi

# Create the file $HOME/.config/rofi/twitch, with one user per line

IFS=$'\r\n' GLOBIGNORE='*' command eval  'options=($(sort $HOME/.config/rofi/twitch))'
options+=(quit)

#echo "${options[@]}"
#echo "${#options[@]}"

rofi_error="$rofi_path -l 2"

if (("${#options[@]}"<10)); then
	rofi_path="$rofi_path -l $(( ${#options[@]} + 2 ))"	
else
	rofi_path="$rofi_path -l 10"
fi

choice=$($rofi_path -dmenu -p 'Watch twitch stream' <<< $(printf '%s\n' "${options[@]}"))

[[ -z $choice || $choice == quit ]] && exit
url="https://www.twitch.tv/$choice"
mpv "$url" \
    --ytdl-format='bestvideo[height<=480]+bestaudio/best[height<=480]' \
    --profile=low-latency --cache=no --untimed > /tmp/twitchoutput.txt

if grep -q offline /tmp/twitchoutput.txt; then
	[[ -z $($rofi_error -dmenu -p "$choice was offline" <<< $(printf "OK")) ]] && exit
elif grep -q "ERROR" /tmp/twitchoutput.txt; then
	[[ -z $($rofi_error -dmenu -p "Some error ocurred" <<< $(printf "OK")) ]] && exit
fi
