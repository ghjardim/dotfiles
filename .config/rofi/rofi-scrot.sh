#!/bin/bash

# Path to save the screenshots
mypath="$HOME/Imagens/screenshots"

# options to be displayed
option0="screen"
option1="area"
option2="window"

# options to be displyed
options="$option0\n$option1\n$option2"

selected="$(echo -e "$options" | rofi -lines 3 -dmenu -p "scrot")"
case $selected in
    "$option0")
        scrot "$mypath"/'%Y-%m-%d-%H%M%S_$wx$h_scrot.png' -e 'optipng $f' \
            && notify-send -t 200 'Screenshot taken!'
        ;;
    "$option1")
        scrot -s "$mypath"/'%Y-%m-%d-%H%M%S_$wx$h_scrot.png' -e 'optipng $f' \
            && notify-send -t 200 'Screenshot taken!'
        ;;
    "$option2")
        scrot -u "$mypath"/'%Y-%m-%d-%H%M%S_$wx$h_scrot.png' -e 'optipng $f' \
            && notify-send -t 200 'Screenshot taken!'\
        ;;
esac
