#!/bin/bash

query=$(rofi -dmenu -p 'Search bookmarks')
if [[ "$query" ]]
then
    list=$(buku --nc -s "$query" -f 4)
else
    list=$(buku --nc -p -f 4)
fi
list=$(echo "$list" | awk -F "\t" '{ print "[" $3 "]", "|", $2}' | sort -u | sed '/^\[\] | \s*/d')

sel=$(echo "$list" | rofi -dmenu -p 'Select' -theme-str 'window {width: 75%;}')

url=$(echo "$sel" | awk -F " | " '{print $NF}')
echo "$url"

[[ "$url" ]] && {
    if ( echo "$url" | xclip -selection clipboard ); then
        notify-send -t 1000 "Copied!" "$url"
    else
        notify-send -t 1000 "Error on copying" "$url"
    fi
}
