#!/bin/bash



#      _     _           _ _       
#  ___| |_  |_|___ ___ _| |_|_____ 
# | . |   | | | .'|  _| . | |     |	Guilherme H. Jardim
# |_  |_|_|_| |__,|_| |___|_|_|_|_|	https://gitlab.com/ghjardim/dotfiles
# |___|   |___|                    	Feel free to copy and modify it.
#
# Rofi script for editing some of my more frequently edited config files.



terminal='xfce4-terminal'

declare -a options=(
"awesome"
"awesome theme"
"bash"
"i3"
"newsboat urls"
"qutebrowser"
#"alacritty"
#"polybar"
"zsh"
"quit"
)

choice=$(echo "$(printf '%s\n' "${options[@]}")" | rofi -dmenu -p 'Edit config file')
echo "You selected $choice"

case "$choice" in
	quit) echo "Program terminated." && exit 1 ;;
	alacritty) file="$HOME/.config/alacritty/alacritty.yml" ;;
    awesome) file="$HOME/.config/awesome/rc.lua" ;;
    "awesome theme") file="$HOME/.config/awesome/themes/gruvbox-arrow/theme.lua" ;;
	bash) file="$HOME/.bashrc" ;;
	i3) file="$HOME/.config/i3/config" ;;
	"newsboat urls") file="$HOME/.config/newsboat/urls" ;;
	polybar) file="$HOME/.config/polybar/config" ;;
	qutebrowser) file="$HOME/.config/qutebrowser/config.py" ;;
    zsh) file="$HOME/.config/zsh/.zshrc" ;;
	*) exit 1 ;;
esac

$terminal -e "vim $file"
