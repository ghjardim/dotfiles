#      _     _           _ _       
#  ___| |_  |_|___ ___ _| |_|_____ 
# | . |   | | | .'|  _| . | |     |	Guilherme H. Jardim
# |_  |_|_|_| |__,|_| |___|_|_|_|_|	https://gitlab.com/ghjardim/dotfiles
# |___|   |___|                    	Feel free to copy and modify it.
#
# My qutebrowser config.
# With UbuntuMono Nerd Font as font, and gruvbox dark as colorscheme.
# Compatible with qutebrowser v2.0.2.




# NOTE: config.py is intended for advanced users who are comfortable
# with manually migrating the config file on qutebrowser upgrades. If
# you prefer, you can also configure qutebrowser using the
# :set/:bind/:config-* commands without having to write a config.py
# file.
#
# Documentation:
#   qute://help/configuring.html
#   qute://help/settings.html


### IMPORTS ###
# Don't know if all those imports are important
import os
import socket
import operator
import subprocess
from qutebrowser.api import interceptor, message
import random

### VARIABLES ###
terminal = os.getenv('TERMINAL') if os.getenv('TERMINAL') is not None else "xfce4-terminal"
deffont = "JetBrains Mono"
mpv_stream_command = "message-info 'mpv (Please wait, it can take a while to load)' ;; hint links spawn mpv {hint-url} --ytdl-format='bestvideo[height<=720,fps<=30]+bestaudio/best[height<=720]'"
ytdl_command = "message-info 'YouTube-DL' ;; hint links spawn " + terminal + " -e \"yt-dlp {hint-url}\""

def get_searx_instance():
    instances=['searx.fmac.xyz', 'search.unlocked.link', 'searx.be', 'search.ononoki.org', 'searx.work', 'searx.tiekoetter.com', 'search.rhscz.eu', 'priv.au']
    return random.choice(instances)

# Instances
invidious_instance = 'invidious.esmailelbob.xyz' #"redirect.invidious.io"    #"yewtu.be"
bibliogram_instance = 'bibliogram.pussthecat.org'
reddit_instance = 'libreddit.kavin.rocks'
searx_instance = get_searx_instance()
#searx_instance = 'searx.envs.net' #"searx.be"
#searx_preferences = ""



### CONFIGURATION ###
# Uncomment this to still load settings configured via autoconfig.yml
config.load_autoconfig(False)

# Setting startpage
c.url.default_page = "https://"+searx_instance+"/"
c.url.start_pages = "https://"+searx_instance+"/"

# Setting dark mode
config.set("colors.webpage.darkmode.enabled", True)
config.set("colors.webpage.darkmode.policy.images", 'never')
config.set("colors.webpage.preferred_color_scheme", 'dark')

# Aliases for commands. (Dict)
c.aliases = {'q': 'quit', 'w': 'session-save', 'wq': 'quit --save',
	'b': 'back', 'mpv': mpv_stream_command, 'ytdl': ytdl_command,
    'js-disable': 'set content.javascript.enabled false',
    'js-enable': 'set content.javascript.enabled true'}

c.downloads.location.directory = '$HOME/Downloads'

#c.tabs.show = 'switching'
c.tabs.position = 'top'
c.tabs.width = '5%'
#c.statusbar.show = 'in-mode'

c.url.searchengines = {
        #'am': 'https://www.amazon.com/s?k={}',
        #'ub': 'https://www.urbandictionary.com/define.php?term={}',
        #'goog': 'https://www.google.com/search?q={}',
        #'wiki': 'https://pt.wikipedia.org/wiki/{}',
        #'yt': 'https://www.youtube.com/results?search_query={}',
        'ddg': 'https://duckduckgo.com/?q={}',
        'ddgh': 'https://html.duckduckgo.com/html?q={}',
        'bibl': 'https://' + bibliogram_instance + '/u/{}',
        'ghub': 'https://github.com/search?q={}',
        'wolf': 'https://www.wolframalpha.com/input/?i={}',
        'dicio': 'https://www.dicio.com.br/{}/',
        'uns': 'https://www.unsplash.com/s/photos/{}',
        'debtr': 'https://tracker.debian.org/pkg/{}',
        'inv': "https://"+invidious_instance+"/search?q={}",

        'en2pt': 'https://www.deepl.com/translator#en/pt/{}',
        'pt2en': 'https://www.deepl.com/translator#pt/en/{}',

        'DEFAULT': "https://"+searx_instance+"/search?q={}",
        'srx': "https://"+searx_instance+"/search?q={}",
        #'DEFAULT': "https://"+searx_instance+"/"+searx_preferences+"&q={}",
        #'srx': "https://"+searx_instance+"/"+searx_preferences+"&q={}"

        # Related to reddit
        #'red': 'https://www.reddit.com/r/{}',
        #'r': 'https://old.reddit.com/r/{}',
        'r': 'https://' + reddit_instance + '/r/{}',
        'rs': 'https://' + reddit_instance + '/search?q={}',
}



### COLORS ###

# base16-qutebrowser theme (https://github.com/theova/base16-qutebrowser)
# Some colors in this files are adapted by ghjardim.

#base00 = "#282828"  #bg
#base01 = "#3c3836"  #bg1
#base02 = "#504945"  #bg2
#base03 = "#665c54"  #bg3
#base04 = "#bdae93"  #fg3
#base05 = "#d5c4a1"  #fg2
#base06 = "#ebdbb2"  #fg1
#base07 = "#fbf1c7"  #fg0
#base08 = "#fb4934"  #red (all colors in lighter shade)
#base09 = "#fe8019"  #orange
#base0A = "#fabd2f"  #yellow
#base0B = "#b8bb26"  #green
#base0C = "#8ec07c"  #aqua
#base0D = "#83a598"  #blue
#base0E = "#d3869b"  #purple
#base0F = "#d65d0e"  #orange (this is dark)

base00 = "#0c0c0c"  #bg
base01 = "#333333"  #bg1
base02 = "#525252"  #bg2
base03 = "#707070"  #bg3
base04 = "#858585"  #fg3
base05 = "#a3a3a3"  #fg2
base06 = "#c2c2c2"  #fg1
base07 = "#f8f8f8"  #fg0
base08 = "#ab4642"  #red (all colors in lighter shade)
base09 = "#f7ca88"  #orange
base0A = "#f7ca88"  #yellow
base0B = "#a1b56c"  #green
base0C = "#7cafc2"  #aqua
base0D = "#7cafc2"  #blue
base0E = "#ba8baf"  #purple
base0F = "#f7ca88"  #orange (this is dark)

# Text color of the completion widget. May be a single color to use for
# all columns or a list of three colors, one for each column.
c.colors.completion.fg = base05

# Background color of the completion widget for odd and even rows.
c.colors.completion.odd.bg = base01
c.colors.completion.even.bg = base00

# Color of completion widget category headers.
c.colors.completion.category.fg = base0A
c.colors.completion.category.bg = base00

# Border color of the completion widget category headers.
c.colors.completion.category.border.top = base00
c.colors.completion.category.border.bottom = base00

# Selected completion item.
c.colors.completion.item.selected.fg = base07
c.colors.completion.item.selected.bg = base03

# Border color of the selected completion item.
c.colors.completion.item.selected.border.top = base02
c.colors.completion.item.selected.border.bottom = base02

# Foreground color of the matched text in the selected completion item.
c.colors.completion.item.selected.match.fg = base0B

# Foreground color of the matched text in the completion.
c.colors.completion.match.fg = base0B

# Scrollbar handle in the completion view.
c.colors.completion.scrollbar.fg = base05
c.colors.completion.scrollbar.bg = base00

# Disabled items in the context menu.
c.colors.contextmenu.disabled.bg = base01
c.colors.contextmenu.disabled.fg = base04

# Context menu. If set to null, the Qt default is used.
c.colors.contextmenu.menu.bg = base00
c.colors.contextmenu.menu.fg =  base05

# Context menu’s selected item. If set to null, the Qt default is used.
c.colors.contextmenu.selected.bg = base02
c.colors.contextmenu.selected.fg = base05

# Download bar.
c.colors.downloads.bar.bg = base00
c.colors.downloads.start.fg = base00
c.colors.downloads.start.bg = base0D
c.colors.downloads.stop.fg = base00
c.colors.downloads.stop.bg = base0C
c.colors.downloads.error.fg = base08

# Font color for hints.
c.colors.hints.fg = base00
# Background color for hints. Note that you can use a `rgba(...)` value
# for transparency.
c.colors.hints.bg = base0A
# Font color for the matched part of hints.
c.colors.hints.match.fg = base05

# Text color for the keyhint widget.
c.colors.keyhint.fg = base05
# Highlight color for keys to complete the current keychain.
c.colors.keyhint.suffix.fg = base05
# Background color of the keyhint widget.
c.colors.keyhint.bg = base00

# Error message.
c.colors.messages.error.fg = base00
c.colors.messages.error.bg = base08
c.colors.messages.error.border = base08

# Warning message.
c.colors.messages.warning.fg = base00
c.colors.messages.warning.bg = base0E
c.colors.messages.warning.border = base0E

# Info message.
c.colors.messages.info.fg = base05
c.colors.messages.info.bg = base00
c.colors.messages.info.border = base00

# Prompts.
c.colors.prompts.fg = base05
c.colors.prompts.bg = base00
# Border used around UI elements in prompts.
c.colors.prompts.border = base00
# Background color for the selected item in filename prompts.
c.colors.prompts.selected.bg = base02

# Statusbar in normal mode
c.colors.statusbar.normal.fg = base0B
c.colors.statusbar.normal.bg = base00

# Statusbar in insert mode.
c.colors.statusbar.insert.fg = base00
c.colors.statusbar.insert.bg = base0D

# Statusbar in passthrough mode.
c.colors.statusbar.passthrough.fg = base00
c.colors.statusbar.passthrough.bg = base0C

# Statusbar in command mode.
c.colors.statusbar.command.fg = base05
c.colors.statusbar.command.bg = base00

# Statusbar in private browsing mode.
c.colors.statusbar.private.fg = base07
c.colors.statusbar.private.bg = base02

# Statusbar in private browsing + command mode.
c.colors.statusbar.command.private.fg = base07
c.colors.statusbar.command.private.bg = base02

# Statusbar in caret mode.
c.colors.statusbar.caret.fg = base00
c.colors.statusbar.caret.bg = base0E

# Statusbar in caret mode with a selection.
c.colors.statusbar.caret.selection.fg = base00
c.colors.statusbar.caret.selection.bg = base0D

# Background color of the progress bar.
c.colors.statusbar.progress.bg = base0D

# Default foreground color of the URL in the statusbar.
c.colors.statusbar.url.fg = base05

# Foreground color of the URL in the statusbar on error.
c.colors.statusbar.url.error.fg = base08

# Foreground color of the URL in the statusbar for hovered links.
c.colors.statusbar.url.hover.fg = base05

# Foreground color of the URL in the statusbar on successful load
# (http and https).
c.colors.statusbar.url.success.http.fg = base0C
c.colors.statusbar.url.success.https.fg = base0B

# Foreground color of the URL in the statusbar when there's a warning.
c.colors.statusbar.url.warn.fg = base0E

# Background color of the tab bar.
c.colors.tabs.bar.bg = base00

# Color gradient start for the tab indicator.
c.colors.tabs.indicator.start = base0D

# Color gradient end for the tab indicator.
c.colors.tabs.indicator.stop = base0C

# Tab indicator on errors.
c.colors.tabs.indicator.error = base08

# Unselected odd tabs.
c.colors.tabs.odd.fg = base05
c.colors.tabs.odd.bg = base01
# Unselected even tabs.
c.colors.tabs.even.fg = base05
c.colors.tabs.even.bg = base00

# Pinned unselected even tabs.
c.colors.tabs.pinned.even.bg = base04
c.colors.tabs.pinned.even.fg = base00
# Pinned unselected odd tabs.
c.colors.tabs.pinned.odd.bg = base05
c.colors.tabs.pinned.odd.fg = base00

# Pinned selected even tabs.
c.colors.tabs.pinned.selected.even.bg = base07
c.colors.tabs.pinned.selected.even.fg = base00
# Pinned selected odd tabs.
c.colors.tabs.pinned.selected.odd.bg = base07
c.colors.tabs.pinned.selected.odd.fg = base00

# Selected odd tabs.
c.colors.tabs.selected.odd.fg = base07
c.colors.tabs.selected.odd.bg = base03
# Selected even tabs.
c.colors.tabs.selected.even.fg = base07
c.colors.tabs.selected.even.bg = base03

# Background color for webpages if unset (or empty to use the theme's
# color).
c.colors.webpage.bg = base00



### FONTS ###

# Default font families to use. Whenever "default_family" is used in a
# font setting, it's replaced with the fonts listed here. If set to an
# empty value, a system-specific monospace default is used.
# Type: List of Font, or Font
c.fonts.default_family = deffont

# Default font size to use. Whenever "default_size" is used in a font
# setting, it's replaced with the size listed here. Valid values are
# either a float value with a "pt" suffix, or an integer value with a
# "px" suffix.
# Type: String
c.fonts.default_size = '11pt'

c.fonts.web.size.minimum = 13
c.fonts.web.size.minimum_logical = 13

# Font used in the completion widget.
# Type: Font
# c.fonts.completion.entry = '11pt "SauceCodePro Nerd Font"'

# Font used for the debugging console.
# Type: Font
# c.fonts.debug_console = '11pt "SauceCodePro Nerd Font"'

# Font used for prompts.
# Type: Font
c.fonts.prompts = 'default_size deffont'

# Font used in the statusbar.
# Type: Font
# c.fonts.statusbar = '11pt "SauceCodePro Nerd Font"'

# Bindings for normal mode
config.bind('m', mpv_stream_command)
config.bind('M', ytdl_command)
config.bind('gs', 'set-cmd-text -s :session-load')
config.bind(',gr', 'config-cycle content.user_stylesheets ~/.config/qutebrowser/solarized-everything-css/css/gruvbox/gruvbox-compiled.css ""')
config.bind('xb', 'config-cycle statusbar.show always in-mode')
config.bind('xt', 'config-cycle tabs.show always never')
config.bind('xx', 'config-cycle statusbar.show always in-mode;; config-cycle tabs.show always switching')

## Tabs
# "sts" for set tab switching, "stl" for set tab left, and so on
# Qutebrowser originally uses s__ for set _ _.
config.bind('sta', 'set tabs.show always')
config.bind('stn', 'set tabs.show never')
config.bind('sts', 'set tabs.show switching')
config.bind('stl', 'set tabs.position left;; set tabs.width 5%;; set tabs.show always')
config.bind('stu', 'set tabs.position top;; set tabs.width 20%;; set tabs.show always')

## Statusbar
config.bind('sba', 'set statusbar.show always')
config.bind('sbi', 'set statusbar.show in-mode')
config.bind('sbn', 'set statusbar.show never')

# Minimizing fingerprinting
#c.content.javascript.enabled = False
c.content.canvas_reading = False
c.content.webgl = False
c.content.headers.accept_language = "en-US, en; q=0.5"
c.content.headers.custom = {"accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"}
c.content.cookies.store = False
c.content.media.audio_capture = False
c.content.blocking.adblock.lists = [
        'https://easylist.to/easylist/easylist.txt', 
        'https://easylist.to/easylist/easyprivacy.txt',
        'https://easylist-downloads.adblockplus.org/easylistdutch.txt',
        'https://easylist-downloads.adblockplus.org/abp-filters-anti-cv.txt', 
        'https://www.i-dont-care-about-cookies.eu/abp/', 
        'https://secure.fanboy.co.nz/fanboy-cookiemonster.txt']

# General user agent: to avoid fingerprinting - Linux-based PC using Chrome browser
config.set('content.headers.user_agent', 'Mozilla/5.0 ({os_info}) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36')

# Specific user agents (Some are Chrome, some are Firefox)
config.set('content.headers.user_agent', 'Mozilla/5.0 ({os_info}) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36', 'https://*.google.com/*')
config.set('content.headers.user_agent', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11.6; rv:93.0) Gecko/20100101 Firefox/93.0', 'https://web.whatsapp.com/*')
config.set('content.headers.user_agent', 'Mozilla/5.0 ({os_info}) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36', 'https://www.facebook.com/*')
config.set('content.headers.user_agent', 'Mozilla/5.0 ({os_info}) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36', 'https://facebook.com/*')
config.set('content.headers.user_agent', 'Mozilla/5.0 ({os_info}; rv:89.0) Gecko/20100101 Firefox/89.0', 'https://www.tumblr.com/*')
config.set('content.headers.user_agent', 'Mozilla/5.0 ({os_info}; rv:89.0) Gecko/20100101 Firefox/89.0', 'https://www.hackerrank.com/*')


### COMPLETION BAR ###
#config.set('completion.cmd_history_max_items', 20)
config.set('completion.web_history.max_items', 20)
config.set('completion.open_categories', ["searchengines", "quickmarks", "history", "bookmarks", "filesystem"])



### PER-DOMAIN SETTINGS ###
# Qutebrowser doesn't support URL patterns for zoom.default yet.
# JavaScript
js_enabled_urls = [
    # Qutebrowser-related
      'chrome-devtools://*',
      'chrome://*/*',
      'devtools://*',
      'qute://*/*',
    # Personal-related
      "*://github.com/*",
      "*://odysee.com/*",
      "*://"+searx_instance+"/*",
      "*://searx.space/*",
      "*://tube.tchncs.de/*",
      "*://web.whatsapp.com/*",
      "*://www.skoob.com.br/*",
      "https://blogdomulin.com.br/*",
      "https://codeberg.org/*",
      "https://duckduckgo.com/*",
      "https://educ.wimelo.com/*",
      "https://gitlab.com/*",
      "https://jitsi.arg.eti.br/*",
      "https://linuxrocks.online/*",
      "https://meet.jit.si/*",
      "https://piped.kavin.rocks/*",
      "https://redirect.invidious.io/*",
      "https://sepiasearch.org/*",
      "https://teddit.net/*",
      "https://wimelo.com/*",
      "https://www.deepl.com/*",
    # Google
      "https://accounts.google.com/*",
      "https://classroom.google.com/*",
      "https://drive.google.com/*",
      "https://mail.google.com/*",
   ]
for url in js_enabled_urls:
    config.set("content.javascript.enabled", True, url)

# Image loading: qutebrowser-related (Bool)
config.set('content.images', True, 'chrome-devtools://*')
config.set('content.images', True, 'devtools://*')

# Notifications (True, False or Ask)
config.set('content.notifications.enabled', False)

# Disable annoying autoplay!!!
config.set('content.autoplay', False)




### ANDRESOUZAABREU'S UNTRACK-URL USERSCRIPT ###
#c.hints.selectors["tracker-url"] = [ "a[href*=\"youtube.com\"]",
#        "a[href*=\"youtu.be\"]", "a[href*=\"twitter.com\"]",
#        "a[href*=\"instagram.com\"]", "a[href*=\"reddit.com\"]",
#        "a[href*=\"maps.google.com\"]" ]
#config.bind(',uc', 'spawn -u untrack-url -r -p {clipboard}')
#config.bind(',uo', 'spawn -u untrack-url -o {url}')
#config.bind(',uor', 'spawn -u untrack-url -r -o {url}')
#config.bind(',uh', 'hint tracker-url spawn -u untrack-url -O {hint-url}')
#config.bind(',uhr', 'hint tracker-url spawn -u untrack-url -r -O {hint-url}')

### TRANSLATE ###
#config.bind(',ts.', 'spawn --userscript translate')
#config.bind(',thl', 'hint userscript link translate')
#config.bind(',tha', 'hint userscript all translate --text')
#config.bind(',tst', 'spawn --userscript translate --text')

### READABILITY ###
c.aliases["read"] = 'spawn --userscript readability'


### INTERCEPTOR ###
# Redirecting hosts
redirecting = True
REDIRECT_MAP = {
    # Medium
    "medium.com": operator.methodcaller('setHost', 'scribe.rip'),
    # Reddit
    "reddit.com": operator.methodcaller('setHost', reddit_instance),
    "www.reddit.com": operator.methodcaller('setHost', reddit_instance),
    "old.reddit.com": operator.methodcaller('setHost', reddit_instance),
    # Twitter
    "twitter.com": operator.methodcaller('setHost', 'nitter.namazso.eu'),
    "mobile.twitter.com": operator.methodcaller('setHost', 'nitter.namazso.eu'),
    # YouTube
    "youtube.com": operator.methodcaller('setHost', invidious_instance),
    "www.youtube.com": operator.methodcaller('setHost', invidious_instance),
    "https://www.youtube.com": operator.methodcaller('setHost', invidious_instance),
    # TikTok
    "tiktok.com": operator.methodcaller('setHost', 'proxitok.herokuapp.com'),
    "www.tiktok.com": operator.methodcaller('setHost', 'proxitok.herokuapp.com'),
    # Imgur
    "imgur.com": operator.methodcaller('setHost', 'imgin.voidnet.tech'),
    # Google
    "translate.google.com": operator.methodcaller('setHost', 'lingva.ml'),
    "google.com": operator.methodcaller('setHost', 'whoogle.sdf.org'),
    # Wikipedia
    "wikipedia.org": operator.methodcaller('setHost', 'wiki.604kph.xyz'),
    "en.wikipedia.org": operator.methodcaller('setHost', 'wiki.604kph.xyz'),
}
def redirect_intercept(info):
    if (info.resource_type != interceptor.ResourceType.main_frame
            or info.request_url.scheme() in {"data", "blob"}):
        return
    url = info.request_url
    redir = REDIRECT_MAP.get(url.host())
    if redir is not None and redir(url) is not False and redirecting is True:
        message.info("Redirecting to " + url.toString())
        info.redirect(url)
interceptor.register(redirect_intercept)
