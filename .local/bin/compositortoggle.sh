#!/bin/sh

#      _     _           _ _       
#  ___| |_  |_|___ ___ _| |_|_____ 
# | . |   | | | .'|  _| . | |     |	Guilherme H. Jardim
# |_  |_|_|_| |__,|_| |___|_|_|_|_|	https://gitlab.com/ghjardim/dotfiles
# |___|   |___|                    	Feel free to copy and modify it.
#
# Shell script that launches the compositor if it is not running,
# or kills it if it is running.

compositor=picom

if pgrep -x $compositor; then 
	pkill -x $compositor
	notify-send "Killed $compositor"
else
	$compositor &
	notify-send "Started $compositor"
fi
