#!/bin/sh


#      _     _           _ _       
#  ___| |_  |_|___ ___ _| |_|_____ 
# | . |   | | | .'|  _| . | |     |	Guilherme H. Jardim
# |_  |_|_|_| |__,|_| |___|_|_|_|_|	https://gitlab.com/ghjardim/dotfiles
# |___|   |___|                    	Feel free to copy and modify it.
#
# Script that launches tmux session "music" with two panes: one for cmus, 
# other for cava.

tmux ls | grep "music" && { tmux a -t music; return 0; }

tmux \
	new-session -s 'music' 'cmus ; read' \; \
	split-window 'cava ; read' \; \
       	select-layout even-horizontal
